package nl.yeswayit.staticexample;

import nl.yeswayit.fastjson.FastJson;

public class Example {

    public static void main(String...args){
        System.out.println("Lazy loading example");
       try {
            Class<?> clazz = Class.forName("nl.yeswayit.fastjson.FastJson");
            FastJson instance = (FastJson) clazz.getConstructor().newInstance();
            System.out.println("Using FastJson");

        } catch (ReflectiveOperationException e) {
            System.out.println("Using Fallback");
        }


    }
}
