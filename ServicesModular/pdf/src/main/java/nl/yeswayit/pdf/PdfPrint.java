package nl.yeswayit.pdf;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class PdfPrint {

    private String label;

    public static List<PdfPrint> getInstances() {
        ServiceLoader<PdfPrint> services = ServiceLoader.load(PdfPrint.class);
        List<PdfPrint> list = new ArrayList<>();
        services.iterator().forEachRemaining(list::add);
        return list;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void print(){
        System.out.println("Base Class calling");
    }
}
