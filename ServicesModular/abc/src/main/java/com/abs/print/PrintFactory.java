package com.abs.print;

import nl.yeswayit.pdf.PdfPrint;

public class PrintFactory {

    public static MsgServiceProviderImpl provider() {
        MsgServiceProviderImpl result = new MsgServiceProviderImpl();
        return result;
    }

    private static class MsgServiceProviderImpl extends PdfPrint {
        public MsgServiceProviderImpl() {
            this.setLabel("SomeImpl through factory");
        }
    }
}
