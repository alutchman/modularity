import com.abs.print.PrintFactory;

module abc.print {
    requires org.pdf;
    provides  nl.yeswayit.pdf.PdfPrint with PrintFactory;
}