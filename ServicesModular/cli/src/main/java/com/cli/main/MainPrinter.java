package com.cli.main;

import nl.yeswayit.pdf.PdfPrint;

import java.util.List;

public class MainPrinter {

    public static void main(String...args){
        List<PdfPrint> msgServices = PdfPrint.getInstances();
        for (PdfPrint msgService : msgServices) {
            System.out.println(msgService.getLabel());
        }
    }
}
