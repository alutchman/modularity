module easytext.gui {
    requires javafx.graphics;
    requires javafx.controls;
    requires easytext.analysis;

    exports  javamodularity.easytext.gui to javafx.graphics;
}