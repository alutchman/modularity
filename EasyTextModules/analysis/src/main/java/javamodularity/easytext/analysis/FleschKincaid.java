package javamodularity.easytext.analysis;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FleschKincaid {
    private String fullText;
    private int numberSentences;
    private int numberOfWords;
    private int sylables;

    private int complexity;

    public FleschKincaid(String textToHandle) {
        this.fullText = textToHandle;
        analyse();
    }

    private void analyse() {
        List<String> sentences =  getSentences();
        numberSentences = sentences.size();

        for (String line : sentences) {
            List<String> subWords = getWords(line);
            numberOfWords += subWords.size();
            for (String word : subWords) {
                sylables += countSyllables(word);
            }
        }

        complexity = calculateCompexity();
    }

    private List<String> getSentences() {
        List<String> result = new ArrayList<>();

        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();   end != BreakIterator.DONE; start = end, end = iterator.next()) {
            result.add(fullText.substring(start, end).trim());
        }
        return result;
    }




    private int calculateCompexity() {
        if (numberSentences > 0 && numberOfWords > 0) {
            double calcValue = 206.835 - (1.015 * ( (double)numberOfWords / (double) numberSentences)) - (84.6 * ( (double)sylables / (double) numberOfWords));

            Long longResult = Math.round(calcValue);
            return longResult.intValue();
        }

        return 0;

    }


    private List<String> getWords(String fullText) {
        List<String> result = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            String subText = fullText.substring(start, end).trim();
            if (subText.length() > 0) {
                if (subText.length() > 1) {
                    result.add(subText);
                } else {
                    boolean foundWeird = Pattern.compile("[!?.]$").matcher(subText).find();
                    if (!foundWeird) {
                        result.add(subText);
                    }
                }
            }
        }

        return result;
    }


    private int countSyllables(String s) {
        final Pattern p = Pattern.compile("([ayeiou]+)");
        final String lowerCase = s.toLowerCase();
        final Matcher m = p.matcher(lowerCase);
        int count = 0;
        while (m.find()) {
            count++;
        }
        if (lowerCase.endsWith("e")) {
            count--;
        }
        int result =  count < 0 ? 1 : count;
        return result;
    }

    public int getNumberSentences() {
        return numberSentences;
    }

    public int getNumberOfWords() {
        return numberOfWords;
    }

    public int getSylables() {
        return sylables;
    }

    public int getComplexity() {
        return complexity;
    }

    public static void main(String... args) {
        String totest = "This order was placed for nobody!\n" +
                "Are you sure? Yes I am.\n" +
                "Thats amazing.\n" +
                "But I am not sure.\n";

        FleschKincaid fleschKincaid = new FleschKincaid(totest);
        System.out.println("Total sentences: " + fleschKincaid.getNumberSentences());
        System.out.println("Total words: " + fleschKincaid.getNumberOfWords());
        System.out.println("Total sylables: " + fleschKincaid.getSylables());

        System.out.println("Total complexity: " + fleschKincaid.getComplexity());
    }

}
