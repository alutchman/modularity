package javamodularity.analysis.api.fleshkincaid;

import javamodularity.easytext.analysis.Analyzer;

import java.text.BreakIterator;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KinCaidAnalyzer implements Analyzer {
    private final static String IDENT = "FleshKincaid";
    private String fullText;
    private int numberSentences;
    private int numberOfWords;
    private int sylables;

    private double complexity;
    private int letters;

    private final LocalDateTime createTime;


    public KinCaidAnalyzer(LocalDateTime createTime) {
        this.createTime = createTime;
    }


    @Override
    public String getName() {
        return IDENT;
    }

    @Override
    public void analyze(String textToHandle) {
        fullText = textToHandle.replaceAll("\n","");
        List<String> sentences =  getSentences();
        numberSentences = sentences.size();

        for (String line : sentences) {
            List<String> subWords = getWords(line);
            numberOfWords += subWords.size();
            for (String word : subWords) {
                sylables += countSyllables(word);
                letters += word.length();
            }
        }

        complexity = calculateCompexity();
    }

    @Override
    public int getNumberSentences() {
        return numberSentences;
    }

    @Override
    public int getLetters() {
        return 0;
    }

    @Override
    public int getNumberOfWords() {
        return numberOfWords;
    }
    @Override
    public int getSylables() {
        return sylables;
    }
    @Override
    public double getComplexity() {
        return complexity;
    }

    private List<String> getSentences() {
        List<String> result = new ArrayList<>();

        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();   end != BreakIterator.DONE; start = end, end = iterator.next()) {
            result.add(fullText.substring(start, end).trim());
        }
        return result;
    }


    private double calculateCompexity() {
        if (numberSentences > 0 && numberOfWords > 0) {
            double calcValue = 206.835 - (1.015 * ( (double)numberOfWords / (double) numberSentences)) - (84.6 * ( (double)sylables / (double) numberOfWords));
            return   calcValue;
        }
        return 0.0;
    }


    private List<String> getWords(String fullText) {
        List<String> result = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            String subText = fullText.substring(start, end).trim();
            if (subText.length() > 0) {
                if (subText.length() > 1) {
                    result.add(subText);
                } else {
                    boolean foundWeird = Pattern.compile("[!?.]$").matcher(subText).find();
                    if (!foundWeird) {
                        result.add(subText);
                    }
                }
            }
        }

        return result;
    }


    private int countSyllables(String s) {
        final Pattern p = Pattern.compile("([ayeiou]+)");
        final String lowerCase = s.toLowerCase();
        final Matcher m = p.matcher(lowerCase);
        int count = 0;
        while (m.find()) {
            count++;
        }
        if (lowerCase.endsWith("e")) {
            count--;
        }
        int result =  count < 0 ? 1 : count;
        return result;
    }


    public static void main(String... args) {
        String totest = "This order was placed for nobody!\n" +
                "Are you sure? Yes I am.\n" +
                "Thats amazing.\n" +
                "But I am not sure.\n";

        KinCaidAnalyzer fleschKincaid = KinCaidAnalyzer.provider();
        fleschKincaid.analyze(totest);
        System.out.println("Total sentences: " + fleschKincaid.getNumberSentences());
        System.out.println("Total words: " + fleschKincaid.getNumberOfWords());
        System.out.println("Total sylables: " + fleschKincaid.getSylables());

        System.out.println("Total complexity: " + fleschKincaid.getComplexity());
    }

    public static KinCaidAnalyzer provider(){
        return new KinCaidAnalyzer(LocalDateTime.now());
    }

}
