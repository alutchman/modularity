module easytext.analyzer.fleshkincaid {
    requires easytext.analysis;

    provides javamodularity.easytext.analysis.Analyzer
            with javamodularity.analysis.api.fleshkincaid.KinCaidAnalyzer;
}