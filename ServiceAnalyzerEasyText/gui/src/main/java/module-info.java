module easytext.gui {
    requires javafx.graphics;
    requires javafx.controls;
    requires easytext.analysis;

    uses javamodularity.easytext.analysis.Analyzer;

    exports  javamodularity.easytext.gui to javafx.graphics;
}