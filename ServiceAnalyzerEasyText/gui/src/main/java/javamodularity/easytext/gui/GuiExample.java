package javamodularity.easytext.gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javamodularity.easytext.analysis.Analyzer;

import java.util.ServiceLoader;

public class GuiExample extends Application {
    private static ComboBox<String> algorithm;
    private static TextArea input;
    private static Text output;

    private Iterable<Analyzer> analyzers = ServiceLoader.load(Analyzer.class);

    @Override
    public void start(Stage stage) {
        stage.setTitle("Easy Text");
        input = new TextArea();
        output = new Text();
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(3));
        vBox.setSpacing(3);
        Text title = new Text("Choose an algorithm:");
        algorithm = new ComboBox<>();
        for(Analyzer analyzer : analyzers) {
            algorithm.getItems().addAll(analyzer.getName());
        }

        Button btn = new Button();
        btn.setText("Calculate");
        btn.setOnAction(event ->
                output.setText(analyze(input.getText(), (String) algorithm.getValue())));


        vBox.getChildren().add(title);
        vBox.getChildren().add(algorithm);
        vBox.getChildren().add(btn);

        BorderPane pane = new BorderPane();
        pane.setRight(vBox);
        pane.setCenter(input);
        pane.setBottom(output);

        stage.setScene(new Scene(pane, 500, 250));

        algorithm.getSelectionModel().select(0);
        stage.show();
    }


    private String analyze(String input, String algorithm) {
        for(Analyzer analyzer : analyzers) {
            if (analyzer.getName().equals(algorithm)) {
                analyzer.analyze(input);
                return "Complexity: "+ analyzer.getComplexity();
            }
        }


        return "Complexity: Undefined algoritm " + algorithm;
    }

    public static void main(String... args) {
        Application.launch(args);
    }
}
