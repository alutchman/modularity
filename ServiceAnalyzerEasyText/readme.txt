mvn clean

del assembly\*.jar

javac -d analysis\target analysis/src/main/java/javamodularity/easytext/analysis/*.java analysis/src/main/java/*.java
jar -cf assembly\easytext.analysis.jar -C  analysis\target .

javac --module-path assembly -d fleshkincaid\target fleshkincaid/src/main/java/javamodularity/analysis/api/fleshkincaid/*.java fleshkincaid/src/main/java/*.java
jar -cf assembly\easytext.analyzer.fleshkincaid.jar -C  fleshkincaid\target .

javac --module-path assembly -d coleman\target coleman/src/main/java/javamodularity/analysis/api/coleman/*.java coleman/src/main/java/*.java
jar -cf assembly\easytext.analyzer.coleman.jar -C  coleman\target .

javac --module-path assembly  -d cli\target cli/src/main/java/javamodularity/easytext/cli/*.java  cli/src/main/java/*.java
jar -cfe assembly\easytext.cli.jar javamodularity.easytext.cli.MainExecution -C cli\target .

java -p assembly --module easytext.cli assembly\test.txt

#=====================Run from target maven
java -p assembly --add-modules easytext.analysis  -jar cli\target\easytext.cli.jar assembly\test.txt

============================GUI===========================================================================================
javac -p "c:\Java\javafx-sdk-11.0.2\lib\;assembly"  -d gui\target gui/src/main/java/javamodularity/easytext/gui/*.java  gui/src/main/java/*.java

jar -cfe assembly\easytext.gui.jar javamodularity.easytext.gui.GuiExample -C gui\target .

java -p "c:\Java\javafx-sdk-11.0.2\lib\;assembly" --add-modules javafx.controls,javafx.fxml,easytext.analysis,easytext.gui  -m easytext.gui


#=====================Run from target maven
java -p "c:\Java\javafx-sdk-11.0.2\lib\;assembly" --add-modules javafx.controls,javafx.fxml,easytext.analysis  -jar gui\target\easytext.gui.jar -Dprism.verbose=true

================= Jlink ==============================================
rmdir /S /Q image
or
rd /S /Q image
jlink -p assembly/;%JAVA_HOME%/jmods --add-modules easytext.cli,easytext.analysis,easytext.analyzer.coleman,easytext.analyzer.fleshkincaid --output image

image\bin\java --list-modules

image\bin\java -m easytext.cli assembly\test.txt
