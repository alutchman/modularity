module easytext.analyzer.coleman {
    requires easytext.analysis;

    provides javamodularity.easytext.analysis.Analyzer
            with javamodularity.analysis.api.coleman.ColemanAnalyzer;
}