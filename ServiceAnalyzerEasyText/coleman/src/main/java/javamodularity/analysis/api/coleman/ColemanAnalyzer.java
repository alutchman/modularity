package javamodularity.analysis.api.coleman;

import javamodularity.easytext.analysis.Analyzer;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColemanAnalyzer implements Analyzer {
    private final static String IDENT = "Coleman Liau-index";
    private String fullText;
    private int numberSentences;
    private int numberOfWords;
    private int sylables;
    private double complexity;
    private int letters;


    @Override
    public String getName() {
        return IDENT;
    }

    @Override
    public void analyze(String textToHandle) {
        fullText = textToHandle.replaceAll("\n","").replaceAll("\r","");
        List<String> sentences =  getSentences();
        numberSentences = sentences.size();

        for (String line : sentences) {
            List<String> subWords = getWords(line);
            numberOfWords += subWords.size();
            for (String word : subWords) {
                sylables += countSyllables(word);
                letters += word.length();
            }
        }

        complexity = calculateCompexity();
    }

    @Override
    public int getNumberSentences() {
        return numberSentences;
    }
    @Override
    public int getNumberOfWords() {
        return numberOfWords;
    }
    @Override
    public int getSylables() {
        return sylables;
    }
    @Override
    public double getComplexity() {
        return complexity;
    }
    @Override
    public int getLetters() {
        return letters;
    }

    private List<String> getSentences() {
        List<String> result = new ArrayList<>();

        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();   end != BreakIterator.DONE; start = end, end = iterator.next()) {
            result.add(fullText.substring(start, end).trim());
        }
        return result;
    }


    private double calculateCompexity() {
        //L = Letters ÷ Words × 100 = 639 ÷ 119 × 100 ≈ 537
        //S = Sentences ÷ Words × 100 = 5 ÷ 119 × 100 ≈ 4.20
        //comp = 0.0588 * L - 0.296 * S - 15.8

        if (numberOfWords > 0) {

            Double averageNumberOfLetters = 100.0 * Math.round( ((double) letters) / ((double)numberOfWords));
            Double averageNumberOfSences  = 100.0 * Math.round( ((double) numberSentences) / ((double)numberOfWords));
            Double comp = 0.0588 * averageNumberOfLetters - 0.296 * averageNumberOfSences - 15.8;
            return Math.floor(comp *100.0) / 100.0;
        }
        return 0.0;
    }


    private List<String> getWords(String fullText) {
        List<String> result = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            String subText = fullText.substring(start, end).trim();
            if (subText.length() > 0) {
                if (subText.length() > 1) {
                    result.add(subText);
                } else {
                    boolean foundWeird = Pattern.compile("[!?.]$").matcher(subText).find();
                    if (!foundWeird) {
                        result.add(subText);
                    }
                }
            }
        }

        return result;
    }


    private int countSyllables(String s) {
        final Pattern p = Pattern.compile("([ayeiou]+)");
        final String lowerCase = s.toLowerCase();
        final Matcher m = p.matcher(lowerCase);
        int count = 0;
        while (m.find()) {
            count++;
        }
        if (lowerCase.endsWith("e")) {
            count--;
        }
        int result =  count < 0 ? 1 : count;
        return result;
    }


    public static void main(String... args) {
        String totest = "This order was placed for nobody!\n" +
                "Are you sure? Yes I am.\n" +
                "Thats amazing.\n" +
                "But I am not sure.\n";

        ColemanAnalyzer colemanAnalyzer = new ColemanAnalyzer();
        colemanAnalyzer.analyze(totest);
        System.out.println("Total sentences: " + colemanAnalyzer.getNumberSentences());
        System.out.println("Total words: " + colemanAnalyzer.getNumberOfWords());
        System.out.println("Total sylables: " + colemanAnalyzer.getSylables());

        System.out.println("Total complexity: " + colemanAnalyzer.getComplexity());
    }

}
