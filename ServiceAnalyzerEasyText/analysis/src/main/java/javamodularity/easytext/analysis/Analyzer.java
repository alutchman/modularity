package javamodularity.easytext.analysis;

import java.util.ServiceLoader;

public interface Analyzer {
    String getName();

    void analyze(String text);

    int getNumberSentences();
    int getLetters();
    int getNumberOfWords();
    int getSylables();
    double getComplexity();

    static Iterable<Analyzer> getAnalyzers(){
        return ServiceLoader.load(Analyzer.class);
    }

}
