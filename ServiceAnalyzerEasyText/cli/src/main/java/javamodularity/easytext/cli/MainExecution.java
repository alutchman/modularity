package javamodularity.easytext.cli;

import javamodularity.easytext.analysis.Analyzer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ServiceLoader;

public class MainExecution {

    public static void main(String... args) {
        if (args != null && args.length >= 1) {
            String filePath = args[0];
            try {
                Path filepath = Paths.get(filePath);
                if (!Files.exists(filepath)) {
                    System.out.println("File does not exist: "+filePath);
                } else {
                    System.out.println("Reading file: "+filePath);
                    String fullText = new String(Files.readAllBytes(filepath));
                    System.out.println("File content: \n"+fullText);

                    Iterable<Analyzer> analyzers = Analyzer.getAnalyzers();
                    for(Analyzer analyzer : analyzers) {
                        analyzer.analyze(fullText);
                        System.out.println("Naam: " + analyzer.getName());
                        System.out.println("Total sentences: " + analyzer.getNumberSentences());
                        System.out.println("Total words: " + analyzer.getNumberOfWords());
                        System.out.println("Total sylables: " + analyzer.getSylables());
                        System.out.println("Total complexity: " + analyzer.getComplexity());
                    }
                }
            } catch (IOException e) {
                System.out.println("File reading failure: "+e.getMessage());
            }
        }
    }
}
