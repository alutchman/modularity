package nl.yeswayit.paths;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PathTest {
    @Test
    public void pathdemo(){
        var expectedP3 = "..\\index.html";
        Path p1 = Paths.get("c:\\personal\\.\\photos\\..\\readme.txt");
        Path p2 = Paths.get("c:\\personal\\index.html");
        //relativize normalized p1 before
        Path p3 = p1.relativize(p2);

        assertEquals(expectedP3, p3.toString());
        assertEquals(p1.resolve(expectedP3).normalize(), p2);
        System.out.println("pathdemo OK");
    }
}
