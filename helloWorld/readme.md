
#To compile 
- javac -d target src/main/java/helloworld/com/javamodule/HelloWorld.java src/main/java/module-info.java
- javac -d target src/main/java/helloworld/com/javamodule/\*.java src/main/java/\*.java

#To run 
- java --module-path ./ --module helloworld/helloworld.com.javamodule.HelloWorld
- java -p ./ --module helloworld/helloworld.com.javamodule.HelloWorld

#Run from jar
- mkdir target\lib
- jar -cfe target\lib\helloworld.jar helloworld.com.javamodule.HelloWorld -C target .
- java --module-path target\lib --module helloworld

#Get module info
java --show-module-resolution --limit-modules java.base  --module-path target\lib --module helloworld

# RESULT
* root helloworld file:///C:/Java/projects/j11ModulesExamples/helloWorld/target/lib/helloworld.jar
* Hello modularized world

#jlink (use jar file in lib) 
- jlink -p target/lib;%JAVA_HOME%/jmods --add-modules helloworld --launcher hello=helloworld --output helloworld-image