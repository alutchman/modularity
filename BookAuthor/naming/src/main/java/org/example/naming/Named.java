package org.example.naming;

public interface Named {
    String getName();
}
