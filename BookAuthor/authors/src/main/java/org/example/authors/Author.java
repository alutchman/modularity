package org.example.authors;

import org.example.book.Book;
import org.example.naming.Named;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Author implements Named  {
    private List<Book> booklist = new ArrayList<>();

    private String name;

    public Author(String name) {
        this.name  = name;
    }


    public List<Book> getBooklist() {
        return booklist;
    }

    public void setBooklist(List<Book> booklist) {
        this.booklist = booklist;
    }

    @Override
    public String toString() {
        String booklistStr  = booklist.stream().map(Book::toString).collect(Collectors.joining(","));
        return "Author{ booklist : {" + booklistStr  +" }, name: " + name + "}";
    }

    @Override
    public String getName() {
        return name;
    }
}
