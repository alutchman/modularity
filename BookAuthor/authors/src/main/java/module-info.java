module bookauthor.authors {
    exports org.example.authors;
    requires bookauthor.book;
    requires transitive bookauthor.naming;

}