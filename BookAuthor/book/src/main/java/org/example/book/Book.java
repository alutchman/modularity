package org.example.book;

import org.example.naming.Named;


public class Book implements Named {
    private String author;

    private String text;

    private String title;

    public Book( String author,  String title,  String text) {
        this.author = author;
        this.text = text;
        this.title = title;
    }

    @Override
    public String getName() {
        return author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
         return "Book{ title: " + title +",  author: " + author + ",  text: " + text +" }";
    }
}
