package org.example.instance;

import org.example.authors.Author;
import org.example.book.Book;

public class ExampleBookAuthor {

    public static void main(String...args) {
        String author = "Jane Eyre";
        Book book1 = new Book(author, "Wuthering Heights", "Some description 1.");
        Book book2 = new Book(author, "The great Depths", "Some description 2.");

        Author author1 = new Author(author);
        author1.getBooklist().add(book1);
        author1.getBooklist().add(book2);

        System.out.println(author1.toString());
    }
}
