rm assembly/output/*.txt
rm assembly/*.jar
rm -rf target
javac -d target src/main/java/easytext/com/javamodule/*.java src/main/java/\*.java
jar -cfe assembly/easytext.jar easytext.com.javamodule.EasyText -C target .
java --module-path assembly --module easytext assembly/test.txt >> assembly/output/result01.txt

