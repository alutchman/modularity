package easytext.com.javamodule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EasyText {
    public static int countSyllables(String s) {
        final Pattern p = Pattern.compile("([ayeiou]+)");
        final String lowerCase = s.toLowerCase();
        final Matcher m = p.matcher(lowerCase);
        int count = 0;
        while (m.find()) {
            count++;
        }
        if (lowerCase.endsWith("e")) {
            count--;
        }
        int result =  count < 0 ? 1 : count;
        return result;
    }



    public static int getCompexity(int numberSentences, int numberOfWords, int sylables) {
        double calcValue = 206.835 - (1.015 * ( (double)numberOfWords / (double) numberSentences)) - (84.6 * ( (double)sylables / (double) numberOfWords));

        Long longResult = Math.round(calcValue);
        return longResult.intValue();
    }


    public static List<String> getWords(String fullText) {
        List<String> result = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            String subText = fullText.substring(start, end).trim();
            if (subText.length() > 0) {
                if (subText.length() > 1) {
                    result.add(subText);
                } else {
                    boolean foundWeird = Pattern.compile("[!?.]$").matcher(subText).find();
                    if (!foundWeird) {
                        result.add(subText);
                    }
                }
            }
        }

        return result;
    }

    public static List<String> getSentences(String fullText) {
        List<String> result = new ArrayList<>();

        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(fullText);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            result.add(fullText.substring(start, end).trim());
        }
        return result;
    }


    public static void main(String... args) {
         if (args != null && args.length >= 1) {
            String filePath = args[0];
            try {
                Path filepath = Paths.get(filePath);
                if (!Files.exists(filepath)) {
                    System.out.println("File does not exist: "+filePath);
                } else {
                    System.out.println("Reading file: "+filePath);
                    String fullText = new String(Files.readAllBytes(filepath));
                    System.out.println("File content: \n"+fullText);
                    handleData(fullText);
                }
            } catch (IOException e) {
                System.out.println("File reading failure: "+e.getMessage());
            }
        }
    }

    private static void handleData(String fullText) {
        List<String> sentences = getSentences(fullText);

        int numberSentences = sentences.size();
        int sylables = 0;
        int numberOfWords = 0;

        for (String line : sentences) {
            List<String> subWords = getWords(line);
            numberOfWords += subWords.size();
            for (String word : subWords) {
                sylables += countSyllables(word);
            }
        }

        System.out.println("Total sentences: " + numberSentences);
        System.out.println("Total words: " + numberOfWords);
        System.out.println("Total sylables: " + sylables);

        int complexity =  getCompexity(numberSentences, numberOfWords, sylables);
        System.out.println("Total complexity: " + complexity);
    }

}
