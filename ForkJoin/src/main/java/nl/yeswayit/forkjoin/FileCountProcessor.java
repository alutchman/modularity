package nl.yeswayit.forkjoin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileCountProcessor extends RecursiveTask<SummaryOfTask> {
    private static final Logger logger = Logger.getLogger(FileCountProcessor.class.getName());
    private final SummaryOfTask summaryOfTask;


    public FileCountProcessor(String pathValue) {
        logger.info(pathValue);
        summaryOfTask = new SummaryOfTask(pathValue);
    }

    public FileCountProcessor(String pathValue, SummaryOfTask parentSummary) {
        logger.info(pathValue);
        summaryOfTask = new SummaryOfTask(pathValue, parentSummary);
    }

    /**
     * When the path has no Subdirectories, return the total files in the subdirectory.
     * Otherwise, go deeper to calculate the files in each subdirectory.
     * @return List<String>
     */
    @Override
    protected SummaryOfTask compute() {
        List<FileCountProcessor> tasks = new ArrayList<>();

        //List to store the names of the files stored in the folder.
        try (var fileStream = Files.list(Paths.get(summaryOfTask.getPathValue())).
                     filter(Files::isReadable).filter(Files::isDirectory)) {
            fileStream.forEach(item -> this.handlePath(item, tasks));
        } catch (IOException e) {
            logger.log(Level.SEVERE,"Access error : {}", e.getMessage());
        }
        try (var fileStream = Files.list(Paths.get(summaryOfTask.getPathValue())).
                filter(Files::isReadable).filter(Files::isRegularFile)) {
            summaryOfTask.setFilesFound(fileStream.count());
        } catch (IOException e) {
            logger.log(Level.SEVERE,"Access error : {}", e.getMessage());
        }


        //If the list of the FolderProcessor subtasks has more than 50 elements,
        //write a message to the console to indicate this circumstance.
        if (tasks.size() > 5) {
            var info = String.format("%s: %d tasks ran.", summaryOfTask.getPathValue(), tasks.size());
             logger.info(info);
        }
        summaryOfTask.setPathsFound(tasks.size());

        //add to the list of files the results returned by the subtasks launched by this task.
        tasks.forEach(FileCountProcessor::join);

        //Return the list of strings
        return this.getSummaryOfTask();
    }

    private void handlePath(Path subPath, List<FileCountProcessor> tasks) {
        if (Files.isDirectory(subPath)){
            FileCountProcessor task = new FileCountProcessor(subPath.toString(), summaryOfTask);
            getSummaryOfTask().getSummaryOfTaskList().add(task.getSummaryOfTask());
            task.fork();
            tasks.add(task);
        }

    }



    public SummaryOfTask getSummaryOfTask() {
        return summaryOfTask;
    }

    @Override
    public String toString() {
        return fetchInfo();
    }

    public String fetchInfo(){
        StringBuilder result = new StringBuilder();
        result.append(getSummaryOfTask()).append("\n");

        for (SummaryOfTask summaryItem  : getSummaryOfTask().getSummaryOfTaskList()) {
            result.append(summaryItem).append("\n");
        }
        return result.toString();
    }

    public static void main(String...args ) {
        var pool = new ForkJoinPool();

        var fileCountProcessor0 = new FileCountProcessor("c:/Temp/");
        var fileCountProcessor1 = new FileCountProcessor("c:/Java/projects/temp/");//c:/Java/projects/temp/ //c:/windows/

        pool.execute(fileCountProcessor0);
        pool.execute(fileCountProcessor1);
        do {
            var info = poolInfo(pool);
            logger.info(info);

        } while (!fileCountProcessor0.isDone() ||  !fileCountProcessor1.isDone());

        pool.shutdown();

        var result01 = fileCountProcessor0.join();
        var result02 = fileCountProcessor1.join();

        var  info01 = result01.toString();
        var info02 = result02.toString();
        logger.info(info01);
        logger.info(info02);
    }

    public static String poolInfo(ForkJoinPool pool){
        StringBuilder result = new StringBuilder();
        result.append("******************************************").append("\n");
        result.append(String.format("Main: Parallelism: %d", pool.getParallelism())).append("\n");
        result.append(String.format("Main: Active Threads: %d", pool.getActiveThreadCount())).append("\n");

        result.append(String.format("Main: Task Count: %d", pool.getQueuedTaskCount())).append("\n");
        result.append(String.format("Main: Steal Count: %d", pool.getStealCount())).append("\n");
        result.append("******************************************").append("\n");

        return result.toString();
    }


}

