package nl.yeswayit.forkjoin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class FolderProcessor extends RecursiveTask<List<Path>> {
    private static final long serialVersionUID = 1L;
    //This attribute will store the full path of the folder this task is going to process.
    private final Path path;
    //This attribute will store the name of the extension of the files this task is going to look for.
    private final String      extension;

    //Implement the constructor of the class to initialize its attributes
    public FolderProcessor(Path path, String extension)  {
        this.path = path;
        this.extension = extension;
    }

    //Implement the compute() method. As you parameterized the RecursiveTask class with the List<String> type,
    //this method has to return an object of that type.
    @Override
    protected List<Path> compute()   {
        //List to store the names of the files stored in the folder.
        List<Path> list = new ArrayList<>();
        //FolderProcessor tasks to store the subtasks that are going to process the subfolders stored in the folder
        List<FolderProcessor> tasks = new ArrayList<>();
        //Get the content of the folder.

        try {
            Files.list(path).filter(this::isReadableDirOrFile).forEach(apath -> handlePath(apath, list, tasks));
        } catch (IOException e) {
            System.err.println("Access error : "+ e.getMessage());
        }

        //If the list of the FolderProcessor subtasks has more than 50 elements,
        //write a message to the console to indicate this circumstance.
        if (tasks.size() > 50) {
            System.out.printf("%s: %d tasks ran.\n", path.toAbsolutePath().toString(), tasks.size());
        }
        //add to the list of files the results returned by the subtasks launched by this task.
        addResultsFromTasks(list, tasks);
        //Return the list of strings
        return list;
    }

    private boolean isReadableDirOrFile(Path aPath) {
        return  !Files.isDirectory(aPath) || Files.isReadable(aPath);
    }

    private void handlePath(Path subPath, List<Path> list, List<FolderProcessor> tasks)  {
        if (Files.isDirectory(subPath)) {
            FolderProcessor task = new FolderProcessor(subPath, extension);
            task.fork();
            tasks.add(task);
        } else {
            if (checkPath(subPath)) {
                list.add(subPath);
            }
        }
    }

    //For each task stored in the list of tasks, call the join() method that will wait for its finalization and then will return the result of the task.
    //Add that result to the list of strings using the addAll() method.
    private void addResultsFromTasks(List<Path> list, List<FolderProcessor> tasks) {
        for (FolderProcessor item : tasks)  {
            list.addAll(item.join());
        }
    }

    //This method compares if the name of a file passed as a parameter ends with the extension you are looking for.
    private boolean checkPath(Path apath) {
        return apath.toString().endsWith(extension);
    }
}