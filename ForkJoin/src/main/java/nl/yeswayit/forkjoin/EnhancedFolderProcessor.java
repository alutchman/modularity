package nl.yeswayit.forkjoin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EnhancedFolderProcessor extends RecursiveTask<Long> {
    private static final long serialVersionUID = -7643091784917999491L;
    private static final Logger logger = Logger.getLogger(EnhancedFolderProcessor.class.getName());

    private final String path;
    //This attribute will store the name of the extension of the files this task is going to look for.
    private final String      extension;

    private AtomicLong counter = new AtomicLong(0L);



    //Implement the constructor of the class to initialize its attributes
    public EnhancedFolderProcessor(Path path, String extension)  {
        this.path = path.toString();
        this.extension = extension;
    }

    public EnhancedFolderProcessor(Path path, String extension, AtomicLong counter)  {
        this.path = path.toString();
        this.extension = extension;
        this.counter = counter;
    }

    public AtomicLong getCounter() {
        return counter;
    }

    private boolean isReadableDirOrFile(Path aPath) {
        return  !Files.isDirectory(aPath) || Files.isReadable(aPath);
    }



    @Override
    protected Long compute() {
        List<Path> list = new ArrayList<>();
        List<EnhancedFolderProcessor> tasks = new ArrayList<>();

        try (var filesToHandleStream = Files.list(Paths.get(path)).filter(this::isReadableDirOrFile)){
            filesToHandleStream.forEach(apath -> handlePath(apath, list, tasks));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Access error ", e);
        }

        //If the list of the FolderProcessor subtasks has more than 50 elements,
        //write a message to the console to indicate this circumstance.
        if (tasks.size() > 50) {
            var info = String.format("%s: %d tasks ran.", path, tasks.size());
            logger.info(info);
        }
        tasks.forEach(EnhancedFolderProcessor::join);

        return counter.get();
    }


    private void handlePath(Path subPath, List<Path> list, List<EnhancedFolderProcessor> tasks)  {
        if (Files.isDirectory(subPath)) {
            var task = new EnhancedFolderProcessor(subPath, extension, counter);
            task.fork();
            tasks.add(task);
        } else {
            if (checkPath(subPath)) {
                list.add(subPath);
                counter.getAndIncrement();
            }
        }
    }

    private boolean checkPath(Path apath) {
        return apath.toString().toLowerCase().endsWith(extension.toLowerCase());
    }

    public static void main(String...args ) {
        //Create ForkJoinPool using the default constructor.
        ForkJoinPool pool = new ForkJoinPool();
        //Create three FolderProcessor tasks. Initialize each one with a different folder path.
        var system = new EnhancedFolderProcessor(Paths.get("C:\\Windows"), ".log");
        var apps = new EnhancedFolderProcessor(Paths.get("C:\\Program Files\\"), ".log");
        var documents = new EnhancedFolderProcessor(Paths.get("c:\\Users\\All Users\\"), "log");
        //Execute the three tasks in the pool using the execute() method.
        pool.execute(system);
        pool.execute(apps);
        pool.execute(documents);
        //Write to the console information about the status of the pool every second
        //until the three tasks have finished their execution.
        do {
            var info = poolInfo(pool);
            logger.info(info);
        } while ((!system.isDone()) || (!apps.isDone()) || (!documents.isDone()));
        //Shut down ForkJoinPool using the shutdown() method.
        pool.shutdown();


        jobInfoLog(system, "System");
        jobInfoLog(apps, "Apps");
        jobInfoLog(documents, "Documents");

    }

    public static void jobInfoLog(EnhancedFolderProcessor task, String label) {
        StringBuilder result = new StringBuilder();
        result.append(String.format("%s: %d", label, task.getCounter().get()));
        var logInf = result.toString();
        logger.log(Level.INFO, logInf);
    }


    public static String poolInfo(ForkJoinPool pool){
        StringBuilder result = new StringBuilder();
        result.append("******************************************").append("\n");
        result.append(String.format("Main: Parallelism: %d", pool.getParallelism())).append("\n");
        result.append(String.format("Main: Active Threads: %d", pool.getActiveThreadCount())).append("\n");

        result.append(String.format("Main: Task Count: %d", pool.getQueuedTaskCount())).append("\n");
        result.append(String.format("Main: Steal Count: %d", pool.getStealCount())).append("\n");
        result.append("******************************************").append("\n");

        return result.toString();
    }
}
