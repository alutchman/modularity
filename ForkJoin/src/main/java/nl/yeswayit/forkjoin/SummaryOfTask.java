package nl.yeswayit.forkjoin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SummaryOfTask implements Serializable {
    private static final long serialVersionUID = -1548801025415301315L;

    private final String pathValue;
    private long filesFound;
    private long pathsFound;

    private SummaryOfTask parent;

    private final List<SummaryOfTask> summaryOfTaskList = new ArrayList<>();

    public SummaryOfTask(String pathValue) {
        this.pathValue = pathValue;
    }

    public SummaryOfTask(String pathValue, SummaryOfTask parent) {
        this.pathValue = pathValue;
        this.parent = parent;
    }

    public String getPathValue() {
        return pathValue;
    }

    public long getFilesFound() {
        return filesFound;
    }

    public long getPathsFound() {
        return pathsFound;
    }

    public void setFilesFound(long filesFound) {
        this.filesFound = filesFound;
    }

    public void setPathsFound(long pathsFound) {
        this.pathsFound = pathsFound;
    }

    public SummaryOfTask getParent() {
        return parent;
    }

    public void setParent(SummaryOfTask parent) {
        this.parent = parent;
    }

    public List<SummaryOfTask> getSummaryOfTaskList() {
        return summaryOfTaskList;
    }

    @Override
    public String toString() {
        return "\n" + infoOfSummary(pathValue);
    }


    private String infoOfSummary(String parentPathValue){
        if (parent != null) {
            var sb = new StringBuilder(this.itemValues(parent.getPathValue()));
            for(var subItem : this.getSummaryOfTaskList()) {
                sb.append("\n").append(subItem.infoOfSummary(parent.getPathValue()));
            }
            return sb.toString();
        } else {
            var sb = new StringBuilder(this.itemValues(parentPathValue));
            for(var subItem : this.getSummaryOfTaskList()) {
                sb.append("\n").append(subItem.infoOfSummary(parentPathValue));
            }
            return sb.toString();
        }
    }

    private String itemValues(String parentPathValue){
        parentPathValue = changeSlashes(parentPathValue);
        var currentPathUse = changeSlashes(pathValue);
        if (currentPathUse.startsWith(parentPathValue) && !currentPathUse.equals(parentPathValue)) {
            var index = parentPathValue.length() ;
            var tmpPathValue = currentPathUse.substring(index);
            return "SummaryOfTask{" +
                    "pathValue='" + tmpPathValue + '\'' +
                    ", filesFound=" + getFilesFound() +
                    ", pathsFound=" + getPathsFound() +
                    '}';
        } else {
            return "SummaryOfTask{" +
                    "pathValue='" + currentPathUse + '\'' +
                    ", filesFound=" + getFilesFound() +
                    ", pathsFound=" + getPathsFound() +
                    '}';
        }
    }

    private static String changeSlashes(String input){
        while (input.indexOf("\\") != -1) {
            input = input.replace("\\","/");
        }
        return input;
    }
}
